import React, { Component } from 'react';

import './Input.scss';

class Input extends Component {
  render() {
    return (
      <div className='input-container'>
        <label className='input-label'>
          {this.props.label}
        </label>
        <input 
          className='input-number' 
          name={this.props.name} 
          type='number' 
          id={this.props.id} 
          onChange={this.props.onChange}/>
      </div>
    );
  }
}
  
export default Input;