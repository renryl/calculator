import React, { Component } from 'react';

import './Radio.scss';

class Radio extends Component {
  render() {
    return (
        <label>
            <input
                className='input-radio'
                type='radio' value={this.props.value}
                checked={this.props.checked}
                onChange={this.props.onChange} />
            {this.props.label}
        </label>
    );
  }
}
  
export default Radio;