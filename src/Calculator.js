import React, { Component } from 'react';
import Input from './Input/Input';
import Radio from './Radio/Radio';

import './Calculator.scss';

class Calculator extends Component {
  state = {
    calculatorInput: [{
      value: undefined,
      label: 'Value 1:',
      id: 1
    }, {
      value: undefined,
      label: 'Value 2:',
      id: 2
    }, {
      value: undefined,
      label: 'Value 3:',
      id: 3
    }],
    methodRadios: [{
      value: 'sum',
      label: 'Sum'
    }, {
      value: 'multiply',
      label: 'Multiply'
    }],
    mathMethod: 'sum',
    result: 0
  };

  // -------- Event management --------------

  onNumberInputChange = event => {
    const targetId = event.target.id;
    const newValue = parseFloat(event.target.value);

    const calculatorInput = this.state.calculatorInput.map((input) => {
      if (input.id === parseInt(targetId)) {
        input.value = newValue;
      }
      return input;
    });

    this.setState({
      calculatorInput,
      result: this.calculateResult(this.state.mathMethod)
    });
  }

  onOptionChange = event => {
    this.setState({
      mathMethod: event.target.value,
      result: this.calculateResult(event.target.value)
    });
  }

  // -------- Calculations --------------

  calculateResult(method) {
    const calculationMethod = {
      sum: this.sum(),
      multiply: this.multiply()
    };
    
    return calculationMethod[method];
  }

  sum() {
    return this.state.calculatorInput.reduce((accumulator, input) => {
      return this.isNumeric(input.value) ? accumulator + input.value : accumulator
    }, 0);
  };

  multiply() {
    let hasValue = false;

    return this.state.calculatorInput.reduce((accumulator, input) => {
      const isNumber = this.isNumeric(input.value);
      if (!hasValue && isNumber) {
        accumulator = 1;
        hasValue = true;
      }
      return isNumber ? accumulator * input.value : accumulator;
    }, 0);
  }

  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  // -------- Rendering --------------

  render() {
    const renderInputs = this.state.calculatorInput.map((input, index) => {
      return ( 
        <div className='card' key={index}>
          <Input label={input.label} name={input.label} id={input.id} onChange={this.onNumberInputChange} /> 
        </div>
      )
    });

    const renderRadios = this.state.methodRadios.map((radio, index) => {
      return (
        <Radio
          value={radio.value}
          onChange={this.onOptionChange}
          label={radio.label}
          checked={this.state.mathMethod === radio.value}
          key={index}/>
      );
    });

    return (
      <div className='calculator-container'>
        {renderInputs}
        <div className='card card--result'>
          <fieldset className='radiogroup'>
            {renderRadios}
          </fieldset>
          <p>Result: {this.state.result}</p>
        </div>
      </div>
    );
  }
}

export default Calculator;